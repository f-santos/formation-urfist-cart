#+TITLE: Formation Urfist "Arbres de décision avec R"
#+SUBTITLE: Instructions d'installation
#+AUTHOR: Frédéric Santos
#+EMAIL: frederic.santos@u-bordeaux.fr
#+OPTIONS: toc:nil email:t
#+LANGUAGE: fr

** Installation de R
*** Pour R lui-même
Pour la formation, nous aurons besoin d'une version raisonnablement récente de R (si possible, version 4.3.0 ou plus récente). Si vous avez déjà une version de R installée sur votre poste mais qu'elle est relativement ancienne, il serait préférable d'installer la dernière version à la place.

Pour installer la dernière version de R :
- *Linux* : des instructions pour les distributions les plus courantes peuvent être trouvées [[https://cran.r-project.org/bin/linux/][sur le site officiel]] ;
- *Mac OS* : des instructions détaillées sont là aussi disponibles [[https://cran.r-project.org/bin/macosx/][sur le site officiel]] ; notez qu'il sera également nécessaire d'installer [[https://www.xquartz.org/][XQuartz]] ;
- *Windows* : installer successivement [[https://cran.r-project.org/bin/windows/base/][le logiciel R]], puis [[https://cran.r-project.org/bin/windows/Rtools/][Rtools]].

*** Installation de packages R
Installer des packages R en amont de la formation sera également nécessaire au cas où nous n'aurions pas une bonne connexion internet sur place. Les packages nécessaires peuvent être installés à l'aide de l'instruction suivante :

#+begin_src R :exports code :eval no
install.packages(c("mice", "partykit", "randomForest", "randomForestExplainer",
                   "rattle", "RColorBrewer", "rpart", "sparkline",
                   "tidyverse", "visNetwork"),
                 dep = TRUE, repos = "https://cran.wu.ac.at/")
#+end_src

** Installation d'un éditeur de texte avancé ou d'un EDI
Cette formation est ouverte à des personnels académiques ayant déjà au moins quelques connaissances élémentaires en langage R. Le choix de l'environnement de travail est donc laissé totalement libre : vous pouvez simplement utiliser votre environnement habituel.

Rappelons toutefois quelques environnements classiques :
- l'EDI [[https://posit.co/download/rstudio-desktop/][Rstudio]] est l'environnement choisi par de nombreux utilisateurs ;
- pour ceux aimant travailler en mode "programmation lettrée", [[https://jupyter.org/][Jupyter Lab]] est une bonne alternative ;
- des éditeurs de texte avancés comme [[https://www.gnu.org/software/emacs/][GNU Emacs]] ou [[https://www.sublimetext.com/][Sublime Text]] ont de bons modes d'édition de code R.

