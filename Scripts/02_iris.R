library(rpart)
library(rpart.plot)

#######################################
### 1. Charger et résumer les données :
iris <- read.csv2(
    file = "data_iris.csv",
    stringsAsFactors = TRUE
)

## Résumé par espèce :
iris |>
    split(f = iris$Species) |>
    lapply(summary)

################################
### 2. Construction de l'arbre :
arbre <- rpart(Species ~ ., data = iris,
               xval = 142, minsplit = 5)

## Affichage avec la fonction de base :
plot(arbre, uniform = TRUE, compress = TRUE, margin = 0.1)
text(arbre, fancy = TRUE, all = TRUE, use.n = TRUE)

## Élagage :
plotcp(arbre) # erreur optimale atteinte pour cp = 0.096
arbre2 <- prune(arbre, cp = 0.096)

## Affichage l'arbre élagué avec la fonction de base :
plot(arbre2, uniform = TRUE, compress = TRUE, margin = 0.1)
text(arbre2, fancy = TRUE, all = TRUE, use.n = TRUE)
## ou bien affichage avec le package {rpart.plot} :
rpart.plot(arbre2)

#################################################
### 3. Prédiction sur un nouveau jeu de données :
ech <- read.csv2("data_iris_pred.csv")
predictions <- predict(arbre2, newdata = ech, type = "class")
print(predictions)
## Matrice de confusion :
table(True = ech$Species, Predicted = predictions)





















